﻿namespace CodeTimer.GUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTimeWorked = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTimeBreaks = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numPomodoroCycle = new System.Windows.Forms.NumericUpDown();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.chckAlwaysOnTop = new System.Windows.Forms.CheckBox();
            this.lblPomCount = new System.Windows.Forms.Label();
            this.lblBreakDebt = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.numPomodoroSBreak = new System.Windows.Forms.NumericUpDown();
            this.numPomodoroLBreak = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.linkPomodoro = new System.Windows.Forms.LinkLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnResetAll = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.btnEyeBreak = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numPomodoroCycle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPomodoroSBreak)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPomodoroLBreak)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(171, 12);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 15);
            this.lblStatus.TabIndex = 4;
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.Location = new System.Drawing.Point(171, 29);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(0, 15);
            this.lblTime.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 15);
            this.label1.TabIndex = 7;
            this.label1.Text = "Time worked";
            // 
            // lblTimeWorked
            // 
            this.lblTimeWorked.AutoSize = true;
            this.lblTimeWorked.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimeWorked.Location = new System.Drawing.Point(214, 41);
            this.lblTimeWorked.Name = "lblTimeWorked";
            this.lblTimeWorked.Size = new System.Drawing.Size(0, 15);
            this.lblTimeWorked.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "Time spend on breaks";
            // 
            // lblTimeBreaks
            // 
            this.lblTimeBreaks.AutoSize = true;
            this.lblTimeBreaks.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimeBreaks.Location = new System.Drawing.Point(214, 87);
            this.lblTimeBreaks.Name = "lblTimeBreaks";
            this.lblTimeBreaks.Size = new System.Drawing.Size(0, 15);
            this.lblTimeBreaks.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(6, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 15);
            this.label5.TabIndex = 11;
            this.label5.Text = "Pomodoro cycle";
            // 
            // numPomodoroCycle
            // 
            this.numPomodoroCycle.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.numPomodoroCycle.Location = new System.Drawing.Point(212, 42);
            this.numPomodoroCycle.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.numPomodoroCycle.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numPomodoroCycle.Name = "numPomodoroCycle";
            this.numPomodoroCycle.Size = new System.Drawing.Size(56, 23);
            this.numPomodoroCycle.TabIndex = 12;
            this.toolTip.SetToolTip(this.numPomodoroCycle, "The productive time. Usualy 25 minutes.");
            this.numPomodoroCycle.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.numPomodoroCycle.ValueChanged += new System.EventHandler(this.numPomodoroCycle_ValueChanged);
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(274, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 15);
            this.label2.TabIndex = 13;
            this.label2.Text = "min";
            // 
            // chckAlwaysOnTop
            // 
            this.chckAlwaysOnTop.AutoSize = true;
            this.chckAlwaysOnTop.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.chckAlwaysOnTop.Location = new System.Drawing.Point(11, 21);
            this.chckAlwaysOnTop.Name = "chckAlwaysOnTop";
            this.chckAlwaysOnTop.Size = new System.Drawing.Size(101, 19);
            this.chckAlwaysOnTop.TabIndex = 14;
            this.chckAlwaysOnTop.Text = "Always on top";
            this.toolTip.SetToolTip(this.chckAlwaysOnTop, "If checked, this window will be allways visible.");
            this.chckAlwaysOnTop.UseVisualStyleBackColor = true;
            this.chckAlwaysOnTop.CheckedChanged += new System.EventHandler(this.chckAlwaysOnTop_CheckedChanged);
            // 
            // lblPomCount
            // 
            this.lblPomCount.AutoSize = true;
            this.lblPomCount.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPomCount.Location = new System.Drawing.Point(7, 23);
            this.lblPomCount.Name = "lblPomCount";
            this.lblPomCount.Size = new System.Drawing.Size(0, 15);
            this.lblPomCount.TabIndex = 15;
            // 
            // lblBreakDebt
            // 
            this.lblBreakDebt.AutoSize = true;
            this.lblBreakDebt.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBreakDebt.Location = new System.Drawing.Point(214, 133);
            this.lblBreakDebt.Name = "lblBreakDebt";
            this.lblBreakDebt.Size = new System.Drawing.Size(0, 15);
            this.lblBreakDebt.TabIndex = 16;
            this.toolTip.SetToolTip(this.lblBreakDebt, "Time that you should spend on breaks.");
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 15);
            this.label4.TabIndex = 17;
            this.label4.Text = "\"Break debt\"";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(7, 90);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 15);
            this.label6.TabIndex = 18;
            this.label6.Text = "Shor break";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(8, 125);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(167, 15);
            this.label7.TabIndex = 19;
            this.label7.Text = "Long break(every 4 pomodori)";
            // 
            // numPomodoroSBreak
            // 
            this.numPomodoroSBreak.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.numPomodoroSBreak.Location = new System.Drawing.Point(212, 83);
            this.numPomodoroSBreak.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numPomodoroSBreak.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPomodoroSBreak.Name = "numPomodoroSBreak";
            this.numPomodoroSBreak.Size = new System.Drawing.Size(56, 23);
            this.numPomodoroSBreak.TabIndex = 22;
            this.toolTip.SetToolTip(this.numPomodoroSBreak, "Break between pomodori.");
            this.numPomodoroSBreak.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numPomodoroSBreak.ValueChanged += new System.EventHandler(this.numPomodoroSBreak_ValueChanged);
            // 
            // numPomodoroLBreak
            // 
            this.numPomodoroLBreak.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.numPomodoroLBreak.Location = new System.Drawing.Point(212, 118);
            this.numPomodoroLBreak.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numPomodoroLBreak.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numPomodoroLBreak.Name = "numPomodoroLBreak";
            this.numPomodoroLBreak.Size = new System.Drawing.Size(56, 23);
            this.numPomodoroLBreak.TabIndex = 23;
            this.toolTip.SetToolTip(this.numPomodoroLBreak, "Every 4 pomodori you should take longer beak.");
            this.numPomodoroLBreak.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numPomodoroLBreak.ValueChanged += new System.EventHandler(this.numPomodoroLBreak_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(274, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 15);
            this.label8.TabIndex = 24;
            this.label8.Text = "min";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(274, 125);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 15);
            this.label9.TabIndex = 25;
            this.label9.Text = "min";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.linkPomodoro);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.numPomodoroCycle);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.numPomodoroLBreak);
            this.groupBox1.Controls.Add(this.lblPomCount);
            this.groupBox1.Controls.Add(this.numPomodoroSBreak);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(12, 219);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(304, 149);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pomodoro";
            this.toolTip.SetToolTip(this.groupBox1, "Application will let you know when pomodoro is finished.");
            // 
            // linkPomodoro
            // 
            this.linkPomodoro.AutoSize = true;
            this.linkPomodoro.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.linkPomodoro.Location = new System.Drawing.Point(199, 0);
            this.linkPomodoro.Name = "linkPomodoro";
            this.linkPomodoro.Size = new System.Drawing.Size(99, 13);
            this.linkPomodoro.TabIndex = 26;
            this.linkPomodoro.TabStop = true;
            this.linkPomodoro.Text = "What is pomodoro?";
            this.linkPomodoro.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkPomodoro_LinkClicked);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.lblTimeWorked);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.lblBreakDebt);
            this.groupBox2.Controls.Add(this.lblTimeBreaks);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(12, 48);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(304, 165);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Summary";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnResetAll);
            this.groupBox3.Controls.Add(this.chckAlwaysOnTop);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox3.Location = new System.Drawing.Point(12, 374);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(304, 53);
            this.groupBox3.TabIndex = 28;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Features";
            // 
            // btnResetAll
            // 
            this.btnResetAll.BackgroundImage = global::CodeTimer.Properties.Resources.reset;
            this.btnResetAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnResetAll.Location = new System.Drawing.Point(262, 18);
            this.btnResetAll.Name = "btnResetAll";
            this.btnResetAll.Size = new System.Drawing.Size(25, 25);
            this.btnResetAll.TabIndex = 18;
            this.toolTip.SetToolTip(this.btnResetAll, "Reset all");
            this.btnResetAll.UseVisualStyleBackColor = true;
            this.btnResetAll.Click += new System.EventHandler(this.btnResetAll_Click);
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 5000;
            this.toolTip.InitialDelay = 1000;
            this.toolTip.ReshowDelay = 100;
            // 
            // btnEyeBreak
            // 
            this.btnEyeBreak.BackgroundImage = global::CodeTimer.Properties.Resources.eye_red;
            this.btnEyeBreak.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEyeBreak.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEyeBreak.Location = new System.Drawing.Point(239, 12);
            this.btnEyeBreak.Name = "btnEyeBreak";
            this.btnEyeBreak.Size = new System.Drawing.Size(30, 30);
            this.btnEyeBreak.TabIndex = 18;
            this.toolTip.SetToolTip(this.btnEyeBreak, "Your eyes need break! Look at objects that are 20 feet away for 20 seonds.");
            this.btnEyeBreak.UseVisualStyleBackColor = true;
            this.btnEyeBreak.Visible = false;
            this.btnEyeBreak.Click += new System.EventHandler(this.btnEyeBreak_Click);
            // 
            // btnSettings
            // 
            this.btnSettings.BackgroundImage = global::CodeTimer.Properties.Resources.Settings_icon;
            this.btnSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSettings.Location = new System.Drawing.Point(284, 12);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(30, 30);
            this.btnSettings.TabIndex = 3;
            this.toolTip.SetToolTip(this.btnSettings, "Switch between view modes.");
            this.btnSettings.UseVisualStyleBackColor = true;
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackgroundImage = global::CodeTimer.Properties.Resources.Media_Controls_stop_icon;
            this.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStop.Location = new System.Drawing.Point(102, 12);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(30, 30);
            this.btnStop.TabIndex = 2;
            this.toolTip.SetToolTip(this.btnStop, "Stop");
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnPause
            // 
            this.btnPause.BackgroundImage = global::CodeTimer.Properties.Resources.coffe_black;
            this.btnPause.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPause.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPause.Location = new System.Drawing.Point(57, 12);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(30, 30);
            this.btnPause.TabIndex = 1;
            this.toolTip.SetToolTip(this.btnPause, "Take a break");
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackgroundImage = global::CodeTimer.Properties.Resources.Media_Controls_play_icon;
            this.btnStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStart.Location = new System.Drawing.Point(12, 12);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(30, 30);
            this.btnStart.TabIndex = 0;
            this.toolTip.SetToolTip(this.btnStart, "Start working");
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 443);
            this.Controls.Add(this.btnEyeBreak);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.btnStart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Take a break";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseDown);
            ((System.ComponentModel.ISupportInitialize)(this.numPomodoroCycle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPomodoroSBreak)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPomodoroLBreak)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTimeWorked;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblTimeBreaks;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numPomodoroCycle;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chckAlwaysOnTop;
        private System.Windows.Forms.Label lblPomCount;
        private System.Windows.Forms.Label lblBreakDebt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numPomodoroSBreak;
        private System.Windows.Forms.NumericUpDown numPomodoroLBreak;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnResetAll;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button btnEyeBreak;
        private System.Windows.Forms.LinkLabel linkPomodoro;
    }
}