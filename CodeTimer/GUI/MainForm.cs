﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CodeTimer.Logic;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace CodeTimer.GUI
{
    public partial class MainForm : Form
    {
        WorkTimer workTimer;
        BreakTimer breakTimer;

        Pomodoro pomodoro;

        bool formOpen = true;

        const int HEIGHT_RESULTMODE = 450;
        const int HEIGHT_WORKMODE = 85;

        bool eyeOnce = false;
        System.Media.SoundPlayer playerEyeBreak = new System.Media.SoundPlayer(Properties.Resources.eyesbreak);
        bool pomoOnce = false;
        System.Media.SoundPlayer playerPomodoroBreak = new System.Media.SoundPlayer(Properties.Resources.pomodorobreak);

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();


        public MainForm()
        {
            InitializeComponent();

            pomodoro = new Pomodoro(
                numPomodoroCycle.Value.ToString(),
                numPomodoroSBreak.Value.ToString(),
                numPomodoroLBreak.Value.ToString());

            workTimer = new WorkTimer("work", pomodoro);
            breakTimer = new BreakTimer("break");

            btnStart.Enabled = false;
        }

        

        private void timer_Tick(object sender, EventArgs e)
        {
            IncreaseTime(workTimer, breakTimer);

            GetResults();
        }

        private void IncreaseTime(WorkTimer w, BreakTimer b)
        {
            w.IncreaseTime();
            b.IncreaseTime();
        }

        private void GetResults()
        {
            if (breakTimer.IsPaused())
            {
                lblTime.Text = workTimer.GetDisplayTime();
                lblStatus.Text = "Work";
            }
            else
            {
                lblTime.Text = breakTimer.GetTime();
                lblStatus.Text = "Break";
            }

            if (workTimer.IsTimeToBreak())
            {
                btnPause.BackgroundImage = Properties.Resources.coffe_red;

                if (!pomoOnce)
                {
                    playerPomodoroBreak.Play();
                    pomoOnce = true;
                }
            }
            else
            {
                btnPause.BackgroundImage = Properties.Resources.coffe_black;
                pomoOnce = false;
            }


            if (workTimer.IsTimeToEyeBreak())
            {
                btnEyeBreak.Visible = true;

                if (!eyeOnce)
                {
                    playerEyeBreak.Play();
                    eyeOnce = true;
                }
            }
            else
            {
                eyeOnce = false;
            }
            
            lblTimeWorked.Text = workTimer.GetDisplayTime();

            lblTimeBreaks.Text = breakTimer.GetTotalTime();

            lblBreakDebt.Text = breakTimer.GetBreakDebt();

            lblPomCount.Text =String.Format( "{0} pomodori done!", Pomodoro.GetCount().ToString() );

        }


        private void ResetAll()
        {
            workTimer = new WorkTimer("work", pomodoro);
            breakTimer = new BreakTimer("break");

            workTimer.Pause();
            breakTimer.Pause();

            ResetLabels(lblBreakDebt, lblTime, lblTimeBreaks, lblTimeWorked);

            btnStart.Enabled = true;
            btnPause.Enabled = false;
            btnStop.Enabled = false;
            btnEyeBreak.Visible = false;
        }


        private void ResetLabels(params Label[] labels)
        {
            for (int i = 0; i < labels.Length; i++)
            {
                labels[i].Text = "00:00:00";
            }
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            workTimer.Pause();
            breakTimer.Resume();

            btnPause.Enabled = false;
            btnStart.Enabled = true;
            btnEyeBreak.Visible = false;
        }

        private void numPomodoroCycle_ValueChanged(object sender, EventArgs e)
        {
            Pomodoro.SetCycle( Convert.ToInt32(numPomodoroCycle.Value) );
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            workTimer.Pause();
            breakTimer.Pause();

            btnStart.Enabled = true;
            btnPause.Enabled = false;
            btnStop.Enabled = false;
            btnEyeBreak.Visible = false;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            workTimer.Resume();
            breakTimer.Pause();

            btnStart.Enabled = false;
            btnPause.Enabled = true;
            btnStop.Enabled = true;
        }

        private void workMode()
        {
            this.Height = HEIGHT_WORKMODE;
            this.FormBorderStyle = FormBorderStyle.None;
            this.Top = Screen.PrimaryScreen.Bounds.Height - 40 - this.Height;
            this.Left = Screen.PrimaryScreen.Bounds.Width - this.Width;
            formOpen = false;
        }

        private void resultsMode()
        {
            this.Height = HEIGHT_RESULTMODE;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.Top = (Screen.PrimaryScreen.Bounds.Height / 2) - this.Height/2;
            this.Left = (Screen.PrimaryScreen.Bounds.Width / 2) - this.Width/2;
            formOpen = true;
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            if (formOpen)
            {
                workMode();
            }
            else
            {
                resultsMode();
            }
        }

        private void chckAlwaysOnTop_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = chckAlwaysOnTop.Checked;
        }

        private void numPomodoroSBreak_ValueChanged(object sender, EventArgs e)
        {
            Pomodoro.SetShortBreak( Convert.ToInt32(numPomodoroSBreak.Value) );
        }

        private void numPomodoroLBreak_ValueChanged(object sender, EventArgs e)
        {
            Pomodoro.SetLongBreak( Convert.ToInt32(numPomodoroLBreak.Value) );
        }

        private void btnResetAll_Click(object sender, EventArgs e)
        {
            ResetAll();
        }

        private void btnEyeBreak_Click(object sender, EventArgs e)
        {
            btnEyeBreak.Visible = false;
        }

        private void MainForm_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void linkPomodoro_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://en.wikipedia.org/wiki/Pomodoro_Technique");
        }
    }
}
