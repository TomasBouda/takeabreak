﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeTimer.Logic
{
    class BreakTimer : MainTimer
    {
        private int debt;

        public BreakTimer(string myName, bool isPaused=true)
        {
            this.Name = myName;
            this.paused = isPaused;
        }

        public override void IncreaseTime()
        {
            if (!paused)
            {
                time += 1000;

                totalTime += 1000;
            }

            SetDebt();
        }

        private void SetDebt()
        {
            int myDebt = Pomodoro.GetBreaksTime() - totalTime;

            if (myDebt < 0)
                debt = 0;
            else
                debt = myDebt;
        }

        public void Pause()
        {
            paused = true;

            time = 0;
        }

        public void Resume()
        {
            paused = false;
        }

        public string GetBreakDebt()
        {
            return millsToStringTime(debt);
        }
    }
}
