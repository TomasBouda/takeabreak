﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeTimer.Logic
{
    class MainTimer
    {

        protected  int time;
        protected int totalTime;
        protected bool paused;
        private string name;

        protected string Name
        {
            get { return name; }
            set { name = value; }
        }

        public MainTimer()
        {

        }

        public MainTimer(string myName, bool isPaused = false)
        {
            this.name = myName;
            this.paused = isPaused;
        }

        public virtual void IncreaseTime()
        {
            if (!paused)
            {
                time += 1000;

                totalTime += 1000;
            }
        }

        public string GetTotalTime()
        {
            return millsToStringTime(totalTime);
        }

        /// <summary>
        /// Converts milliseconds to standard time representation
        /// </summary>
        /// <param name="milliseconds">Time in milliseconds</param>
        /// <returns>Time in format 00:00:00</returns>
        protected string millsToStringTime(int milliseconds)
        {
            TimeSpan t = TimeSpan.FromMilliseconds(milliseconds);

            string timeHours = string.Format("{0:D2}:{1:D2}:{2:D2}",
                                    t.Hours,
                                    t.Minutes,
                                    t.Seconds);

            return timeHours;
        }

        public string GetTime()
        {
            return millsToStringTime(time);
        }

        public bool IsPaused()
        {
            return paused;
        }
    }
}
