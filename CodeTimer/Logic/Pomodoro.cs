﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeTimer.Logic
{
    class Pomodoro
    {
        private static int cycle;
        private static int count;
        private static int breakTime;
        private static int longBreakTime;

        public Pomodoro()
        {
            cycle = 25 * 60 * 1000;
            count = 0;
            breakTime = 5 * 60 * 1000;
            longBreakTime = 10 * 60 * 1000;
        }

        public Pomodoro(int myCycle, int myBreakTime, int myLongBreakTime)
        {
            cycle = myCycle * 60 * 1000;
            count = 0;
            breakTime = myBreakTime * 60 * 1000;
            longBreakTime = myLongBreakTime * 60 * 1000;
        }

        public Pomodoro(string myCycle, string myBreakTime, string myLongBreakTime)
        {
            cycle = Convert.ToInt32(myCycle) * 60 * 1000;
            count = 0;
            breakTime = Convert.ToInt32(myBreakTime) * 60 * 1000;
            longBreakTime = Convert.ToInt32(myLongBreakTime) * 60 * 1000;
        }

        public static void SetCycle(int myCycle)
        {
            cycle = myCycle * 60 * 1000;
        }

        public void CalculateCount(int workTotalTime)
        {
            count = workTotalTime / cycle;
        }

        public static void SetShortBreak(int time)
        {
            breakTime = time * 60 * 1000;
        }

        public static void SetLongBreak(int time)
        {
            longBreakTime = time * 60 * 1000;
        }

        public static int GetBreaksTime()
        {
            if (count >= 4)
            {
                return (count / 4) * longBreakTime + (count - (count / 4)) * breakTime;
            }
            else
            {
                return count * breakTime;
            }
        }

        public static int GetCount()
        {
            return count;
        }

        public int GetCycle()
        {
            return cycle;
        }

    }
}
