﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeTimer.Logic
{
    class WorkTimer : MainTimer
    {
        Pomodoro pomodoro;

        private bool timeToBreak = false;

        private const int EYE_BREAK_DURATION = 20;
        private int eyeBreakTimer;
        private bool timeToEyeBreak = false;

        private int displayTime;


        public WorkTimer(string myName, bool isPaused=false)
        {
            this.Name = myName;
            this.paused = isPaused;
            this.pomodoro = new Pomodoro();
            this.eyeBreakTimer = 0;
        }

        public WorkTimer(string myName, Pomodoro myPomodoro, bool isPaused = false)
        {
            this.Name = myName;
            this.paused = isPaused;
            this.pomodoro = myPomodoro;
            this.eyeBreakTimer = 0;
        }

        public override void IncreaseTime()
        {
            if (!paused)
            {
                time += 1000;

                displayTime = totalTime + time;

                if (time >= pomodoro.GetCycle())
                    timeToBreak = true;

                pomodoro.CalculateCount(displayTime);

                CheckEyes();
            }
            
        }

        private void CheckEyes()
        {
            eyeBreakTimer += 1000;

            if (eyeBreakTimer == EYE_BREAK_DURATION * 60 * 1000)
                timeToEyeBreak = true;
            else 
            {
                if (eyeBreakTimer == (EYE_BREAK_DURATION * 60 * 1000) + 20000)
                {
                    timeToEyeBreak = false;
                    eyeBreakTimer = 0;
                }
                timeToEyeBreak = false;
            }
        }

        public void Pause()
        {
            paused = true;
            timeToBreak = false;

            totalTime += time;
            time = 0;

            eyeBreakTimer = 0;
        }

        public void Resume()
        {
            paused = false;
        }

        public bool IsTimeToBreak()
        {
            return timeToBreak;
        }

        public bool IsTimeToEyeBreak()
        {
            return timeToEyeBreak;
        }

        public string GetDisplayTime()
        {
            return millsToStringTime(displayTime);
        }
    }
}
