# TakeABreak #

## What is TakeABreak? ##

As the application name suggests, this is an application that helps track the time you worked and suggests, when to get a little break. Also every 20 minutes it highlights the need to let your eyes relax. Applications is built around the concept [pomodoro](https://en.wikipedia.org/wiki/Pomodoro_Technique), with the difference that the app **only warns**, that is time to take a break, which is useful when you are programming.

![tab.png](https://bitbucket.org/repo/M7GXqR/images/1724054792-tab.png)

### Contacts ###

email@tomasbouda.cz